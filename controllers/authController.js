const express = require('express')
const axios = require('axios')
const jwt = require('jsonwebtoken')
const app = express()
const bcrypt = require('bcryptjs')

const userServices = process.env.USER_URI || `http://localhost:313`

app.set('secretKey', 'nodeRestApi')

const handleResponseSuccess = (resultUser, res) => {
  if (resultUser?.user) {
    let token = jwt.sign(
      {
        id: resultUser._id,
        email: resultUser.email,
        username: resultUser.username,
        userRole: resultUser.role,
      },
      app.get('secretKey'),
      { expiresIn: '1y' },
    )
    return res.status(200).json({
      status: 'success',
      data: {
        ...resultUser,
        user: {
          _id: resultUser.user._id,
          username: resultUser.user.username,
          role: resultUser.user.role,
          email: resultUser.user.email,
          token,
        },
      },
    })
  } else {
    let token = jwt.sign(
      {
        id: resultUser._id,
        email: resultUser.email,
        username: resultUser.username,
        userRole: resultUser.role,
      },
      app.get('secretKey'),
      { expiresIn: '1y' },
    )
    return res.status(202).json({
      status: 'User Not Found',
      data: { user: resultUser, token },
    })
  }
}

exports.login = function (req, res, next) {
  const { username, password } = req.body
  axios
    .get(`${userServices}/user?username=${username}`)
    .then((dataUser) => {
      const resultUser = dataUser.data.length > 0 ? dataUser.data[0] : null

      if (resultUser !== null) {
        if (bcrypt.compareSync(password, resultUser?.password)) {
          axios
            .get(`${userServices}/profile`, {
              params: { userid: resultUser._id },
            })
            .then((valProfile) => {
              const resProfile =
                valProfile.data.length > 0 ? valProfile.data[0] : null

              let mapvalprofile = {
                _id: resProfile?._id,
                name: resProfile?.name,
                gender: resProfile?.gender,
                phone: resProfile?.phone,
                photo: resProfile?.photo,
                nik: resProfile?.nik,
                jobrole: resProfile?.jobrole,
                status: resultUser.status,
              }

              let token = jwt.sign(
                {
                  id: resultUser._id,
                  email: resultUser.email,
                  username: resultUser.username,
                  userRole: resultUser.role,
                },
                app.get('secretKey'),
                { expiresIn: '1y' },
              )
              res.status(200).json({
                status: 'success',
                data: {
                  ...mapvalprofile,
                  user: {
                    _id: resultUser._id,
                    username: resultUser.username,
                    role: resultUser.role,
                    email: resultUser.email,
                    token,
                  },
                },
              })
            })
            .catch((err) => {
              console.log('error get user', err)
              res.json({ message: err })
            })
        } else {
          console.log('password salah')
          res.status(400).json({ err, message: 'password salah!' })
        }
      } else {
        console.log('else resultuser null')
        res.status(400).json({ err, message: 'failed!' })
      }
    })
    .catch((err) => {
      console.log('error get user', err)
      res.status(400).json({ err, message: 'failed!' })
    })
}

exports.logout = function (req, res, next) {
  const { username, password } = req.body
  axios
    .get(`${userServices}/user?username=${username}&password=${password}`)
    .then((dataUser) => {
      const resultUser = dataUser.data.length > 0 ? dataUser.data[0] : null
      handleResponseSuccess(resultUser, res)
    })
    .catch((err) => {
      console.log('error get user', err)
      res.status(400).json({ err, message: 'failed!' })
    })
}
exports.updateToken = function (req, res, next) {
  const { username, password, token } = req.body

  const handlePatchToken = (params) => {
    axios
      .patch(
        `${userServices}/user?username=${params?.username}&password=${params?.password}`,
        { token: params?.token },
      )
      .then((dataUser) => {
        const resultUser = dataUser.data.length > 0 ? dataUser.data[0] : null
        handleResponseSuccess(resultUser, res)
      })
      .catch((err) => {
        console.log('error get user', err)
        res.status(400).json({ err, message: 'failed!' })
      })
  }

  if (username && password) {
    handlePatchToken({ username, password, token })
  } else {
    axios
      .get(userServices + '/user', {
        params: { token: req.headers?.authtoken },
      })
      .then((resultUser) => {
        handlePatchToken(resultUser)
      })
      .catch((err) => {
        console.log('error', err)
      })
  }
}
